//鼠标移入事件
	function _onmouseover(node){

		node.style.background="url(./images/images/mouseover_bg_03.gif) no-repeat";
		node.style.backgroundPosition="center 44px";
	}

	//鼠标移出事件
	function _onmouseout(node){
		node.style.background="none";
	}

	// 鼠标移入提交按钮
	function sub_onmouseover(node){
		node.style.backgroundColor="#45566a";
	}

	//鼠标移出提交按钮
	function sub_onmouseout(node){
		node.style.backgroundColor="rgb(60,60,60)";
	}
	//自适应屏幕高度
	function resizeWindow(){
		var winH = document.documentElement.scrollHeight || document.body.scrollHeight;
		var marH = winH * 0.7;
		var marW = winH * 0.2;
		var node = document.getElementById("susppend");
		node.children[0].style.marginTop = marH + 'px';
		node.children[0].style.marginLeft = marW + 'px';
		var nodeH = winH - marH;
		node.style.height=winH+'px';
	}

	// 邮件发送系统
	document.getElementsByTagName('form')[0].onsubmit=function(evt){
		// 验证表单
		var in_node = document.getElementsByTagName('input');
		var text_node = document.getElementsByTagName('textarea');
		if(in_node[0].value == '' || in_node[1].value == '' ||in_node[2].value == '' || text_node[0].value == ''){
			evt.preventDefault();
		}else{
			var xhr = new XMLHttpRequest();
			var info = new FormData(this);
			xhr.onreadystatechange=function(){
				if(xhr.readyState == 4){
					if(xhr.responseText == 'true'){
						console.log(xhr.responseText);
						var node = document.getElementById('susppend');
						node.children[0].innerHTML='留言提交成功，我们会尽快回复您！';
						node.style.display='block';
						node.style.zIndex='3';
					}else{
						console.log(xhr.responseText);
						var node = document.getElementById('susppend');
						node.children[0].innerHTML='留言提交失败，请刷新后再重试！';
						node.style.display='block';
						node.style.zIndex='3';
					}
					resizeWindow();
				}
			}
			setTimeout(function(){
			var node = document.getElementById('susppend').style.display = 'none';
			},1500);
			evt.preventDefault();
			xhr.open('post',"./phpfile/send.php");
			xhr.send(info);
		}
	} 
